# gym-dssat-pdi spack repository

This is a [Spack](https://spack.io/) repository with recipes for building a [gym-dssat-pdi](https://gitlab.inria.fr/rgautron/gym_dssat_pdi) environment.

Using Spack, gym-dssat-pdi can be installed in a few simple steps:
1. [setup spack](#step-1-setup),
2. [install](#step-2-installation).

## Creating gym-dssat-pdi Spack environment
#### Step #1: Setup

To use it, you should first setup spack:
```sh
# 1. Get and enable Spack (version 0.19.0, last stable when writing this text)
git clone --branch v0.19.0 -c feature.manyFiles=true https://github.com/spack/spack.git
. spack/share/spack/setup-env.sh

# 2. Get and enable PDI spack repo
git clone https://github.com/pdidev/spack.git spack/var/spack/repos/pdi
spack repo add spack/var/spack/repos/pdi

# 3. Get and enable this spack repo
git clone https://gitlab.inria.fr/rgautron/gym_dssat_pdi-spack.git spack/var/spack/repos/gym-dssat-pdi
spack repo add spack/var/spack/repos/gym-dssat-pdi
```


#### Step #2: Installation

Installation is as simple as:
```sh
spack install gym-dssat
```


#### Step #3: Create gym-dssat-pdi Spack environment

Spack environments make it easier to use the software installed via Spack. The recipe for the gym-dssat-pdi environment we have prepared is the file ```spack.yaml``` provided in ```spack/var/spack/repos/gym-dssat-pdi/env```. Once activated, this environment provides a specific view of the gym-dssat-pdi installation in ```spack/gym-dssat-pdi```.

To create and activate the environment:
```sh
# Create the Spack environment from the recipe
spack env create gym-dssat-pdi spack/var/spack/repos/gym-dssat-pdi/env/spack.yaml
```

A view has been created for this envinronment in spack/gym-dssat-pdi

## Using gym-dssat-pdi Spack environment
#### Step #1: Get easy access to Spack commands
In this example, Spack has been installed in the user home: ```/home/emilioj/```.
```sh
. /home/emilioj/spack/share/spack/setup-env.sh
```

#### Step #2A: Activate it
```sh
spack env activate gym-dssat-pdi

You can check it is activated with
spack env status
```

#### (Step #2B: Concretize the environment to get a unified/integrated 'view' of gym-dssat-pdi)
This is only needed the first time or whenever we introduce changes in the environment!

```sh
spack concretize
```

#### Step #3: Use gym-dssat-pdi Spack environment
Once we have an activated concretized environment we can use gym-dssat-pdi, in a similar way to using python venvs.

For example:

```sh
$ which python
/home/emilioj/spack/gym-dssat-pdi/bin/python

$ python
>>> import gym
>>> env = gym.make('gym_dssat_pdi:GymDssatPdi-v0')
```
Once the environment is activated, we can install any additional Python packages you need with ```pip```.

We can also run the example bundled with gym-dssat:
```sh
$ python spack/gym-dssat-di/samples/run_env.py
```

#### Step #4: Dectivate it
To go back to your normal Python environment, type:
```sh
spack env deactivate
```
