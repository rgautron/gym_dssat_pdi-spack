# Copyright: 2021-23, Romain Gautron and Emilio J. Padron
# License:   BSD-3-Clause

from spack import *
import os
import shutil

class GymDssat(PythonPackage):
    """An OpenAI Gym crop management environment for Reinforcement Learning (RL), based on the Decision Support System for Agrotechnology Transfer (DSSAT) crop model."""

    homepage = "https://gitlab.inria.fr/rgautron/gym_dssat_pdi"
    url      = "https://gitlab.inria.fr/rgautron/gym_dssat_pdi/-/archive/v0.0.8/gym_dssat_pdi-v0.0.8.tar.bz2"
    git      = "https://gitlab.inria.fr/rgautron/gym_dssat_pdi"

    maintainers = ['rgautron', 'emilioj']

    version('0.0.8', sha256='a97276f43ce0b6ea9e543367fde9cc1f74c20a6155cc652048593c3376e6b9f6')
    version('0.0.7', sha256='f9e52642c0fafa7d44e243d745eb33aef78684842ef98601c0a6b7ab6fc1f503')

    depends_on('python@3.8:', type=('build', 'link', 'run'))
    depends_on('py-setuptools@46.4:', type='build')
    depends_on('dssat-pdi', type=('build', 'run'))
    depends_on('py-numpy@1.24.1', type=('build', 'run'), when="@0.0.8")
    depends_on('py-numpy@1.21.6', type=('build', 'run'), when="@0.0.7")
    depends_on('py-matplotlib@3.3.4', type=('build', 'run'))
    depends_on('py-jinja2@3.0.3', type=('build', 'run'))
    depends_on('py-pyzmq@25.0.0', type=('build', 'run'), when="@0.0.8")
    depends_on('py-pyzmq@22.3.0', type=('build', 'run'), when="@0.0.7")
    depends_on('py-psutil@5.9.2', type=('build', 'run'))
    depends_on('py-pyyaml@6.0', type=('build', 'run'))
    depends_on('py-six@1.16.0', type=('build', 'run'))
    depends_on('py-gym@0.21.0', type=('build', 'run'), when="@0.0.8")
    depends_on('py-gym@0.19.0', type=('build', 'run'), when="@0.0.7")

    build_directory = 'gym-dssat-pdi'

    # Change run_dssat path in run_env.py
    patch("remove_rundssat_path.patch", when="@0.0.7")
    patch("find_rundssat_in_path.patch", when="@0.0.7")

    @run_after('install')
    def post_install(self):
        """Create symlink samples->lib/python3.x/site-packages/gym_dssat_pdi_samples"""
        install_dir = self.spec.prefix
        python_ver = os.listdir(os.path.join(install_dir, 'lib'))[0]
        sample_dir = install_dir + '/lib/' + python_ver + '/site-packages/gym_dssat_pdi_samples'
        symlink = os.path.relpath(sample_dir, install_dir)

        os.symlink(symlink, 'samples')
        shutil.move('samples', install_dir)
