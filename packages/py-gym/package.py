# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)
#
# Modified by Emilio J. Padrón to override original py-gym package
# to include version 0.19.0 of Gym in the recipe

from spack.pkg.builtin.py_gym import PyGym as BuiltinPyGym

class PyGym(BuiltinPyGym):

    version('0.21.0', sha256="0fd1ce165c754b4017e37a617b097c032b8c3feb8a0394ccc8777c7c50dddff3")

    depends_on("py-numpy@1.18.0:", type=("build", "run"), when="@0.21.0")
