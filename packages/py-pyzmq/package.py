# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)
#
# Modified by Emilio J. Padrón to override original py-pyzmq package
# to include version 25.0.0 of pyzmq in the recipe

from spack.pkg.builtin.py_pyzmq import PyPyzmq as BuiltinPyPyzmq

class PyPyzmq(BuiltinPyPyzmq):

    version('25.0.0', sha256="f330a1a2c7f89fd4b0aa4dcb7bf50243bf1c8da9a2f1efc31daf57a2046b31f2")
