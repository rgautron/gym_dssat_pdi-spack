# Copyright: 2021-23, Romain Gautron and Emilio J. Padron
# License:   BSD-3-Clause

from spack import *
import os
import shutil

class DssatPdi(CMakePackage):
    """DSSAT modification for gym-DSSAT RL environment"""

    homepage = "https://gitlab.inria.fr/rgautron/gym_dssat_pdi"
    url      = "https://gitlab.inria.fr/rgautron/dssat-csm-os/-/archive/0.2.4.7/dssat-csm-os-0.2.4.7.tar.bz2"
    git      = "https://gitlab.inria.fr/rgautron/dssat-csm-os"

    maintainers = ['rgautron', 'emilioj']

    version('4.8.0.24_2', sha256='410193834540d41831c37b847a3219f6f70386135647389e8429c0c62bfb3a27')
    version('4.8.0.24_1', sha256='41cdf4fff9c50e26501f50508c0e2123b911148e2fdc827b8299e5a121b4a630')
    version('0.3.4.7', sha256='ddb06723f8afe0440a0e8fde92832f08c0733dfcd7edb3b915c434f48b61ef0a')
    version('0.2.4.7', sha256='54590a2158a40271c78b2fc97e1e5173c573342845ee46a9ef70ad04dc71ed63')

    depends_on('pdi@1.5.0: ^python@3.8: ^spdlog+fmt_external')
    depends_on('pdiplugin-pycall')
    depends_on('pkgconfig', type=('build'))

    resource(
        name='dssat-csm-data',
        git='https://github.com/DSSAT/dssat-csm-data.git',
        # commit='c1a31af16fc82659e0b024d58e40b021981b182b',
        tag='v4.8.0.28',
        destination='dssat-csm-data'
    )

    variant(
        'build_type', default='Release',
        description='The build type to build',
        values=('Debug', 'Release', 'Testing')
    )

    @run_after('install')
    def post_install(self):
        """Copy dssat-csm-data after installing DSSAT"""
        dst = self.spec.prefix
        for key in self.resources:
            for res in self.resources[key]:
                src = res.fetcher.stage.source_path
                print('POST_INSTALL: copy DSSAT data files from ', src, ' to ', dst)
                print(os.listdir(src))
                print(os.listdir(dst))
                for f in os.listdir(src):
                    old = os.path.join(src, f)
                    new = os.path.join(dst, f)
                    print(old, ' => ', new)
                    if os.path.isfile(old):
                        shutil.copy(old, new)
                    else:
                        shutil.copytree(old, new)

        #TEMP workaround to avoid long-path DSSAT issue
        #* Modify path in DSSATPRO.L48 and run_dssat:
        #  spack/opt/spack/*/*/dssat-pdi-*/ -> spack/gym-dssat-pdi/bin
        filter_file(r'/spack/opt/spack/.*/.*/dssat-pdi-[^/^\s]*',
                    '/spack/gym-dssat-pdi/bin', os.path.join(dst, 'run_dssat'))
        filter_file(r'/spack/opt/spack/.*/.*/dssat-pdi-[^/^\s]*',
                    '/spack/gym-dssat-pdi/bin', os.path.join(dst, 'DSSATPRO.L48'))
